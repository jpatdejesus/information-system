@extends('layout')

@section('content')

	<form method="POST" action="{{route('owner-manager.store')}}">
		@csrf
		<div>
			<lable>Firstname</lable>
			<input type="text" name="firstname" id="firstname" />
		</div>
		<div>
			<lable>Lastname</lable>
			<input type="text" name="lastname" id="lastname" />
		</div>
		<div>
			<lable>Nickname</lable>
			<input type="text" name="nickname" id="nickname" />
		</div>
		<div>
			<lable>Position</lable>
			<select name="position" id="position">
				<option>Select Position</option>
				<option value="Owner">Owner</option>
				<option value="Manager">Manager</option>
			</select>
		</div>
		<div>
			<button type="submit" class="btn btn-primary">Save</button>
		</div>
	</form>

@endsection