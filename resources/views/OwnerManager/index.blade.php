@extends('layout')

@section('content')

	<a href="/owner-manager/create">Add New</a>
	<table>
		<thead>
			<tr>
				<th>Firstname</th>
				<th>Lastname</th>
				<th>Nickname</th>
				<th>Position</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
			@foreach($details as $detail)
			<tr>
				<td>{{$detail->firstname}}</td>
				<td>{{$detail->lastname}}</td>
				<td>{{$detail->nickname}}</td>
				<td>{{$detail->position}}</td>
				<td>
					<a href="/owner-manager/edit/{{$detail->id}}">Edit</a>
					<a href="/owner-manager/delete/{{$detail->id}}">Delete</a>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>

@endsection