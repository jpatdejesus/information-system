@extends('layout')

@section('content')

	<form method="POST" action="{{route('owner-manager.update', ['id' => $id])}}">
		@csrf
		<div>
			<lable>Firstname</lable>
			<input type="text" name="firstname" id="firstname" value="{{$detail->firstname}}" />
		</div>
		<div>
			<lable>Lastname</lable>
			<input type="text" name="lastname" id="lastname" value="{{$detail->lastname}}" />
		</div>
		<div>
			<lable>Nickname</lable>
			<input type="text" name="nickname" id="nickname" value="{{$detail->nickname}}" />
		</div>
		<div>
			<lable>Position</lable>
			<select name="position" id="position">
				<option>Select Position</option>
				<option value="Owner">Owner</option>
				<option value="Manager">Manager</option>
			</select>
		</div>
		<div>
			<button type="submit" class="btn btn-primary">Save</button>
		</div>
	</form>

@endsection