<?php

/**
 * ===================================
 * Config file for SI - Information System
 * ===================================
 */
return [
    /*
    |--------------------------------------------------------------------------
    | SIIS REQUEST FORM TYPE
    |--------------------------------------------------------------------------
    |
    | Configuration of SIIS request form type
    |
    */
    'request_type' => [
        'leave_form' => 'LEAVE_FORM_REQUEST',
        'change_shift_form' => 'CHANGE_SHIFT_REQUEST',
        'overtime_request' => 'OVERTIME_REQUEST',
    ],
    'password_regex' => env('PASSWORD_REGEX', '/^(?=.*\d)(?=.*[@#$%^&!\*]?)(?=.*[a-z])(?=.*[A-Z])[0-9A-Za-z@#$%^&!\*]/i'),
    'username_regex' => env('USERNAME_REGEX', '/^[A-Za-z0-9\.\_]+$/'),
];