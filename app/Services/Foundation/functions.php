<?php

if (!function_exists('sanitize_url')) {
    /**
     * Sanitize URL. Add trailing slash.
     *
     */
    function sanitize_url($url, $slug = '')
    {
        return rtrim($url, '/') . '/' . ltrim($slug, '/');
    }
}

if (!function_exists('array_keys_exists')) {
    /**
     * compare if all keys exist
     */
    function array_keys_exists(array $keys, array $compare)
    {
        return !array_diff_key(array_flip($keys), $compare);
    }
}

if (!function_exists('check_production')) {
    /**
     * @return bool
     */
    function check_production()
    {
        if (config('app.env') === 'production' && config('app.debug') === false) {
            return true;
        }

        return false;
    }
}

if (!function_exists('make_sqlite_files')) {
    function make_sqlite_files()
    {
        if (!file_exists(database_path('stub.sqlite'))) {
            $file = fopen(database_path('stub.sqlite'), 'w');
            fclose($file);
        }

        if (!file_exists(database_path('testing.sqlite'))) {
            $file = fopen(database_path('testing.sqlite'), 'w');
            fclose($file);
        }
    }
}
