<?php

namespace App\Providers;

use App\Rules\NameRegexRule;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Modules\AccountManagement\Rules\UsernameRegexRule;
use Modules\AccountManagement\Rules\PasswordRegexRule;
use Modules\AccountManagement\Rules\NicknameRegexRule;

class ValidatorServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    public function boot()
    {
        Validator::extendImplicit('name_regex', function ($attribute, $value, $parameters) {
            return (new NameRegexRule())->passes($attribute, $value);
        });
        Validator::extendImplicit('username_regex', function ($attribute, $value, $parameters) {
            return (new UsernameRegexRule())->passes($attribute, $value);
        });
        Validator::extend('password_regex', function ($attribute, $value, $parameters, $validator) {
            return (new PasswordRegexRule())->passes($attribute, $value);
        });
        Validator::extend('nickname_regex', function ($attribute, $value, $parameters, $validator) {
            return (new NicknameRegexRule())->passes($attribute, $value);
        });
    }
}
