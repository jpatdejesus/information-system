<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class InformationSystemInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'siis:install {--force}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute migrations and other dependencies';

    /**
     * Execute the console command.
     *
     * @return int Number of transaction that are expired
     */
    public function handle()
    {
        if (!$this->option('force')) {
            $confirmation = $this->ask(
                'The following command will execute.' . PHP_EOL .
                ' 1. php artisan migrate:refresh' . PHP_EOL .
                ' 2. php artisan db:seed' . PHP_EOL .
                ' 3. php artisan key:generate' . PHP_EOL .
                ' 4. php artisan document-manager:init' . PHP_EOL .
                ' Do you wish to continue? (y/N)'
            );

            if (strtolower($confirmation) !== 'y') {
                $this->info('Installation has been cancelled');
                return false;
            }
        }

        $this->info(PHP_EOL . 'Migrating...' . PHP_EOL);
        $this->call('migrate:refresh');
        $this->info(PHP_EOL . 'Seeding...' . PHP_EOL);
        $this->call('db:seed');
        $this->info(PHP_EOL . 'Generating app key...' . PHP_EOL);
        $this->call('key:generate');
        $this->info(PHP_EOL . 'Initializing document manager...' . PHP_EOL);
        $this->call('document-manager:init');

        return true;
    }
}
