<?php

namespace Modules\Timekeeping\Repositories;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\BaseController;
use App\Repositories\Repository;
use Modules\Timekeeping\Entities\Timekeeping;
use Modules\AccountManagement\Entities\UserDetail;
use Modules\TaskManagement\Entities\Task;

class TimekeepingRepository
{
    protected $timekeeping;
    protected $userDetail;
    protected $task;

    public function __construct(Timekeeping $timekeeping, UserDetail $userDetail, Task $task)
    {
        $this->timekeeping = $timekeeping;
        $this->userDetail = $userDetail;
        $this->task = $task;
    }

    const VALIDATION_MESSAGES = [
        'RECORD_LIST' => 'Record list.',
        'NO_RECORD' => 'No records.',
        'RECORD_NOT_FOUND' => 'Record not found.',
        'SUCCESS_CREATED' => 'Success to create record!',
        'SUCCESS_UPDATED' => 'Success to update record!',
        'FAILED_CREATE' => 'Failed to create record!',
        'FAILED_UPDATE' => 'Failed to update record!',
        'SYSTEM_ERROR' => 'An error occured. Please contact admin.'
    ];

    protected $selectedUserDetailAttributes =  [
        'user_details.user_id',
        'user_details.photo',
        'user_details.first_name',
        'user_details.middle_name',
        'user_details.last_name',
        'user_details.nickname',
        'user_details.job_title',
        'user_details.level',
        'user_details.shift',
        'user_details.date_hired',
        'user_details.town',
        'user_details.city',
        'user_details.province',
        'user_details.mobile_number',
        'user_details.birth_date'
    ];

    protected $selectedLogAttributes =  [
        'log_details.id',
        'log_details.work_date',
        'log_details.time_in',
        'log_details.time_out'
    ];

    protected $validCreateAttributes =  [
        'user_id',
        'work_date',
        'time_in',
        'time_out'
    ];

    protected $validUpdateAttributes =  [
        'id',
        'time_out'
    ];

    protected $selectRaw1 = "CONCAT(
                                TIME_FORMAT(TIMEDIFF(time_out, time_in), '%H'), 
                                ' Hours', 
                                ' ', 
                                TIME_FORMAT(TIMEDIFF(time_out, time_in), '%i'), 
                                ' Minutes'
                            ) AS total_log_hours";

    protected $selectRaw2 = "(CASE 
                                WHEN TIME_TO_SEC(TIMEDIFF(time_out, time_in)) >= 36000  
                                        THEN 
                                            '10 Hours'
                                    ELSE 
                                            CONCAT(
                                                TIME_FORMAT(TIMEDIFF(time_out, time_in), '%H'), 
                                                ' Hours', 
                                                ' ', 
                                                TIME_FORMAT(TIMEDIFF(time_out, time_in), '%i'), 
                                                ' Minutes') END
                            ) AS total_working_hours";

    public $response = [];

    public function buildResponse($data = [], $message = null)
    {
        $this->response['data'] = $data;
        $this->response['message'] = $message;
    }

    /**
     * Search user with logs
     * @param user_id - optional
     * @param level - optional
     * @param job_title - optional
     * @param keyword - optional
     * @return List of user
     */
    public function searchUserWithLogs($params)
    {
        $data = array();

        /**
         * Get user by user_id (logged user) or by params
         */
        $query = $this->userDetail->select($this->selectedUserDetailAttributes);
        
        if (isset($params['user_id'])) {
            $query->where('user_id', $params['user_id']);
        } else {
            if (isset($params['level']) && $params['level']) {
                $query->whereIn("level", $params['level']);
            }

            if (isset($params['job_title']) && $params['job_title']) {
                $query->whereIn('job_title', $params['job_title']);
            }

            if (isset($params['keyword']) && $params['keyword']) {
                $keyword = $params['keyword'];
                $query->whereRaw(
                    "first_name like ? OR middle_name like ? OR last_name like ? OR nickname like ? OR job_title like ?",
                    array('%'.$keyword.'%', '%'.$keyword.'%', '%'.$keyword.'%', '%'.$keyword.'%','%'.$keyword.'%')
                );
            }
        }

        $users = $query->get();

        if ($users->toArray()) {
            foreach ($users->toArray() as $key => $value) {
                $data[$key] = $value;
                $data[$key]['logs'] = [];
                $data[$key]['tasks'] = [];
                $data[$key]['time'] = [];
            }

            $this->buildResponse($data, self::VALIDATION_MESSAGES['RECORD_LIST']);
        } else {
            $this->buildResponse([], self::VALIDATION_MESSAGES['NO_RECORD']);
        }

        return $this->response;
    }

    /**
     * Get user logs filter by user_id and date range
     * Get total task group by status filter by user_id
     * Get total time group by status
     * @param $user_id - required
     * @param date_from - optional
     * @param date_to - optional
     * @return List of logs
     * @return Total task group by status
     * @return Total time (CODE REVIEW, QA TEST, UAT, DONE)
     */
    public function getDetails($params)
    {
        $user_id = isset($params['user_id']) ? $params['user_id'] : null;
        $date_from = isset($params['date_from']) ? $params['date_from'] : null;
        $date_to = isset($params['date_to']) ? $params['date_to'] : null;
        $data = array();

        /**
         * GET LOGS
         */
        $logs = $this->timekeeping->select($this->selectedLogAttributes)
            ->selectRaw($this->selectRaw1)
            ->selectRaw($this->selectRaw2)
            ->where('user_id', $user_id);

        if ($date_from && $date_to) {
            $logs->whereRaw("work_date BETWEEN ? AND ?", array($date_from, $date_to));
        }

        $temp_logs = $logs->orderBy('user_id', 'asc')
            ->orderBy('work_date', 'asc')
            ->get();

        /**
         * GET TOTAL TASK BY STATUS
         */
        $task = $this->task->selectRaw('
            projects.name AS project_name, 
            projects.description AS project_description, 
            COUNT(tasks.user_id) AS total_task');

        foreach (config('taskmanagement.status') as $value) {
            $task->selectRaw("SUM(CASE WHEN task_details.status = '".$value."' THEN 1 ELSE 0 END) AS '".$value."'");
        }

        if ($date_from && $date_to) {
            $task->where('task_details.start_date', '>=', $date_from);
            $task->where('task_details.start_date', '<=', $date_to);
        }

        $temp_task = $task->join('task_details', 'task_details.task_id', '=', 'tasks.id')
            ->leftJoin('modules', 'modules.id', '=', 'tasks.module_id')
            ->leftJoin('project_modules', 'project_modules.module_id', '=', 'modules.id')
            ->leftJoin('projects', 'projects.id', '=', 'project_modules.project_id')
            ->where('tasks.user_id', $user_id)
            ->groupBy('projects.id')
            ->groupBy('projects.name')
            ->groupBy('projects.description')
            ->get();


         /**
         * GET TOTAL TIME BY STATUS
         */
        $time = $this->task->selectRaw('
            projects.name AS project_name, 
            projects.description AS project_description');

        $validStatus = array("CODE REVIEW", "QA TEST", "UAT", "DONE");

        foreach (config('taskmanagement.status') as $value) {
            if (in_array($value, $validStatus)) {
                $time->selectRaw("SUM(CASE WHEN task_details.status = '".$value."' THEN actual_hours ELSE 0 END) AS '".$value."'");
            }
        }

        $time->selectRaw("SUM(CASE WHEN task_details.status IN (".sprintf("'%s'", implode("', '", $validStatus)).") THEN actual_hours ELSE 0 END) AS 'total_actual_hours'");


        if ($date_from && $date_to) {
            $time->where('task_details.start_date', '>=', $date_from);
            $time->where('task_details.start_date', '<=', $date_to);
        }

        $temp_time = $time->join('task_details', 'task_details.task_id', '=', 'tasks.id')
            ->leftJoin('modules', 'modules.id', '=', 'tasks.module_id')
            ->leftJoin('project_modules', 'project_modules.module_id', '=', 'modules.id')
            ->leftJoin('projects', 'projects.id', '=', 'project_modules.project_id')
            ->where('tasks.user_id', $user_id)
            ->groupBy('projects.id')
            ->groupBy('projects.name')
            ->groupBy('projects.description')
            ->get();

        $data['logs'] = $temp_logs->toArray();
        $data['tasks'] = $temp_task->toArray();
        $data['time'] = $temp_time->toArray();

        $this->buildResponse($data, self::VALIDATION_MESSAGES['RECORD_LIST']);
        return $this->response;
    }

    /**
     * Store a newly created resource in storage.
     * @param $request
     * @return New record
     */
    public function saveRecord($request)
    {
        $params = $request->only($this->validCreateAttributes);
        
        if ($params) {
            $data = $this->timekeeping->create($params);

            if ($data) {
                $this->buildResponse($data->toArray(), self::VALIDATION_MESSAGES['SUCCESS_CREATED']);
            } else {
                $this->buildResponse([], self::VALIDATION_MESSAGES['FAILED_CREATE']);
            }
        }

        return $this->response;
    }

    /**
     * Update the specified resource in storage.
     * @param $request
     * @param int $id
     * @return Updated record
     */
    public function updateRecord($request)
    {
        $params = $request->only($this->validUpdateAttributes);

        if ($params) {
            $details = $this->timekeeping->find($params['id']);

            if ($details) {
                $details->time_out = $params['time_out'];

                if ($details->save()) {
                    $data = $this->timekeeping->select($this->selectedLogAttributes)
                                        ->selectRaw($this->selectRaw1)
                                        ->selectRaw($this->selectRaw2)
                                        ->whereRaw("id = " . $params['id'])
                                        ->get();

                    $this->buildResponse($data->toArray(), self::VALIDATION_MESSAGES['SUCCESS_UPDATED']);
                } else {
                    $this->buildResponse([], self::VALIDATION_MESSAGES['FAILED_UPDATE']);
                }
            } else {
                $this->buildResponse([], self::VALIDATION_MESSAGES['RECORD_NOT_FOUND']);
            }
        }

        return $this->response;
    }
}
