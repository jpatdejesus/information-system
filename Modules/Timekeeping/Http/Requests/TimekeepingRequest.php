<?php

namespace Modules\Timekeeping\Http\Requests;

use App\Http\Requests\BaseRequest;

class TimekeepingRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        switch ($this->method()) {
            case 'POST':
                return [
                    'user_id' => 'required|numeric',
                    'work_date' => 'required|date',
                    'time_in' => 'required|regex:/(\d+\:\d+)/',
                    'time_out' => 'nullable|regex:/(\d+\:\d+)/'
                ];
                break;
            case 'PUT':
                return [
                    'time_out' => 'required|regex:/(\d+\:\d+)/'
                ];
                break;
        }
    }

    public function messages()
    {
        return [
            '*.required' => 'The :attribute field is required.',
            '*.numeric' => 'The :attribute field is invalid.',
            '*.date' => 'The :attribute field is invalid.',
            '*.regex' => 'The :attribute field is invalid.'
        ];
    }
}
