<?php

namespace Modules\Timekeeping\Entities;

use Illuminate\Database\Eloquent\Model;

class Timekeeping extends Model
{
    /**
     * primaryKey
     *
     * @var integer
     * @access protected
     */
    protected $guarded = ['id'];

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'log_details';

    protected $fillable = [
        'user_id',
        'work_date',
        'time_in',
        'time_out'
    ];

}
