<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\WithFaker;
use Modules\TaskManagement\Entities\Owner;

class OwnerUnitTest extends TestCase
{
    use WithoutMiddleware;
    use WithFaker;

    /**
     * @test
     */
    public function it_should_get_all_owner()
    {
        $response = $this->get('/api/task-management/owner');
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'List of Owners.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_create_owner()
    {
        $response = $this->post('/api/task-management/owner', $this->createValidParams());
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Owner created successfully.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_update_a_owner()
    {
        $response = $this->put('/api/task-management/owner', $this->updateValidParams());
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Owner updated successfully.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_get_a_owner()
    {
        $owner = Owner::inRandomOrder()->first();

        $response = $this->get('/api/task-management/owner/' . $owner->id);
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Owner request successful.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_delete_a_owner()
    {
        $owner = factory(Owner::class)->create();

        $response = $this->delete('/api/task-management/owner/' . $owner->id);
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Owner deleted successfully.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_test_required_first_name()
    {
        $field = 'first_name';
        $validErrorResponse = [
            "The first name is required.",
        ];

        /**
         * Test if first_name is not null
         */
        $this->assertions($validErrorResponse, $field);
    }

    /**
     * @test
     */
    public function it_should_test_required_last_name()
    {
        $field = 'last_name';
        $validErrorResponse = [
            "The last name is required.",
        ];

        /**
         * Test if last_name is not null
         */
        $this->assertions($validErrorResponse, $field);
    }

    private function createValidParams()
    {
        return [
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'position' => $this->faker->word()
        ];
    }

    private function updateValidParams()
    {
        $owner = Owner::first();

        return [
            'id' => $owner->id,
            'first_name' => $this->faker->firstName(),
            'last_name' => $this->faker->lastName(),
            'position' => $this->faker->word()
        ];
    }

    private function createInvalidParam($testField, $value = null)
    {
        $return = [
            'first_name' => '',
            'last_name' => ''
        ];

        if ($value !== null) {
            $return[$testField] = $value;
        } else {
            unset($return[$testField]);
        }

        return $return;
    }


    private function assertions(array $errorMessages, $field, $param = null)
    {
        if (!$param) {
            $response = $this->json('POST', '/api/task-management/owner', $this->createInvalidParam($field));
        } else {
            $response = $this->json('POST', '/api/task-management/owner', $this->createInvalidParam($field, $param));
        }
        
        $response->assertStatus(422)
            ->assertJson([
                "message" => true,
                "errors" => true
        ]);

        $content = json_decode($response->getContent());
        $this->assertSame('The given data was invalid.', $content->message);

        foreach ($content->errors->{$field} as $error) {
            $this->assertTrue(in_array($error, $errorMessages));
        }
    }
}
