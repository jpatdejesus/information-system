<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\WithFaker;
use Modules\TaskManagement\Entities\ProjectMember;
use Modules\TaskManagement\Entities\Project;

class ProjectMemberUnitTest extends TestCase
{
    use WithoutMiddleware;
    use WithFaker;

    /**
     * @test
     */
    public function it_should_get_project_with_members()
    {
        $project = factory(Project::class)->create();

        $response = $this->get('/api/task-management/projectmember/project/' . $project->id);
        $response->assertStatus(200
)            ->assertJson([
                'message' => 'Project with Member request successful.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_get_member_with_projects()
    {
        $project_member = factory(ProjectMember::class)->create();

        $response = $this->get('/api/task-management/projectmember/member/' . $project_member->id);
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Member with Projects request successfully.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_remove_member_from_project()
    {
        $project_member = factory(ProjectMember::class)->create();

        $response = $this->delete(
            '/api/task-management/projectmember/' . $project_member->project_id,
            [
                'member_id' => $project_member->member_id
            ]
        );
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Remove Member from Project successful.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_add_member_to_project()
    {
        $response = $this->post('/api/task-management/projectmember', $this->createValidParams());
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Add Member to Project successful.'
            ]);
    }

    private function createValidParams()
    {
        return [
            'member_id' => $this->faker->randomDigitNot(0),
            'project_id' => $this->faker->randomDigitNot(0)
        ];
    }
}
