<?php

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\WithFaker;
use Modules\TaskManagement\Entities\Module;

class ModuleUnitTest extends TestCase
{
    use WithoutMiddleware;
    use WithFaker;

    /**
     * @test
     */
    public function it_should_get_all_module()
    {
        $response = $this->get('/api/task-management/module');
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'List of Modules.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_create_module()
    {
        $response = $this->post('/api/task-management/module', $this->createValidParams());
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Module created successfully.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_update_a_module()
    {
        $response = $this->put('/api/task-management/module', $this->updateValidParams());
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Module updated successfully.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_get_a_module()
    {
        $module = Module::inRandomOrder()->first();

        $response = $this->get('/api/task-management/module/' . $module->id);
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Module request successful.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_delete_a_module()
    {
        $module = factory(Module::class)->create();

        $response = $this->delete('/api/task-management/module/' . $module->id);
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Module deleted successfully.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_assign_module_to_user()
    {
        $response = $this->patch('/api/task-management/module/assign', $this->assignValidParams());
        $response->assertStatus(200)
            ->assertJson([
                'message' => 'Module assignment successful.'
            ]);
    }

    /**
     * @test
     */
    public function it_should_test_required_name()
    {
        $field = 'name';
        $validErrorResponse = [
            "The name is required.",
        ];

        /**
         * Test if name is not null
         */
        $this->assertions($validErrorResponse, $field);
    }

    /**
     * @test
     */
    public function it_should_test_required_description()
    {
        $field = 'description';
        $validErrorResponse = [
            "The description is required.",
        ];

        /**
         * Test if description is not null
         */
        $this->assertions($validErrorResponse, $field);
    }

    private function createValidParams()
    {
        return [
            'name' => $this->faker->name(),
            'description' => $this->faker->text(30),
            'user_id' => 1,
            'project_id' => 3,
            'deadline' => $this->faker->date('Y-m-d')
        ];
    }

    private function updateValidParams()
    {
        $module = Module::first();

        return [
            'id' => $module->id,
            'name' => $this->faker->words(2, true),
            'description' => $this->faker->text(30),
            'user_id' => $this->faker->randomDigitNot(0),
            'deadline' => $this->faker->date('Y-m-d')
        ];
    }

    private function assignValidParams()
    {
        $module = Module::first();

        return [
            'id' => $module->id,
            'user_id' => $this->faker->randomDigitNot(0)
        ];
    }

    private function createInvalidParam($testField, $value = null)
    {
        $return = [
            'name' => '',
            'description' => ''
        ];

        if ($value !== null) {
            $return[$testField] = $value;
        } else {
            unset($return[$testField]);
        }

        return $return;
    }


    private function assertions(array $errorMessages, $field, $param = null)
    {
        if (!$param) {
            $response = $this->json('POST', '/api/task-management/module', $this->createInvalidParam($field));
        } else {
            $response = $this->json('POST', '/api/task-management/module', $this->createInvalidParam($field, $param));
        }

        $response->assertStatus(422)
            ->assertJson([
                "message" => true,
                "errors" => true
        ]);

        $content = json_decode($response->getContent());
        $this->assertSame('The given data was invalid.', $content->message);

        foreach ($content->errors->{$field} as $error) {
            $this->assertTrue(in_array($error, $errorMessages));
        }
    }
}
