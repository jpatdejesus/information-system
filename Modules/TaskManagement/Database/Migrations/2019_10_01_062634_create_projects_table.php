<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('description', 250);
            $table->unsignedInteger('owner_id')->nullable();
            $table->unsignedInteger('manager_id')->nullable();
            $table->date('deadline')->nullable();
            // $table->foreign('owner_id')->references('id')->on('owners');
            // $table->foreign('manager_id')->references('id')->on('managers');
            $table->index(['name']);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
