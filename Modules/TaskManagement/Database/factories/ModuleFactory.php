<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(Modules\TaskManagement\Entities\Module::class, function (Faker $faker) {
    return [
        'name' => $faker->words(2, true),
        'description' => $faker->text(30),
        'user_id' => $faker->randomDigitNot(0),
        'deadline' => $faker->dateTime()
    ];
});
