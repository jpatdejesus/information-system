<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(Modules\TaskManagement\Entities\ProjectMember::class, function (Faker $faker) {
    return [
        'project_id' => factory(Modules\TaskManagement\Entities\Project::class)->create()->id,
        'member_id' => $faker->randomDigitNot(0)
    ];
});
