<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(Modules\TaskManagement\Entities\ProjectModule::class, function (Faker $faker) {
    return [
        'project_id' => factory(Modules\TaskManagement\Entities\Project::class)->create()->id,
        'module_id' => factory(Modules\TaskManagement\Entities\Module::class)->create()->id
    ];
});
