<?php

Route::group(['prefix' => 'module'], function () {
    Route::get('', 'ModuleController@getAllModule');
    Route::get('/{id}', 'ModuleController@getModule');
    Route::post('', 'ModuleController@createModule');
    Route::put('', 'ModuleController@updateModule');
    Route::delete('/{id}', 'ModuleController@deleteModule');
    Route::patch('/assign', 'ModuleController@assignModuleToUser');
});
