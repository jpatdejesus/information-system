<?php

Route::group(['prefix' => 'manager'], function () {
    Route::get('', 'ManagerController@getAllManager');
    Route::get('/{id}', 'ManagerController@getManager');
    Route::delete('/{id}', 'ManagerController@deleteManager');
    Route::post('', 'ManagerController@createManager');
    Route::put('', 'ManagerController@updateManager');
});
