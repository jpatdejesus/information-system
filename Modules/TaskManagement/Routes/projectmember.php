<?php

Route::group(['prefix' => 'projectmember'], function () {
    Route::get('/project/{id}', 'ProjectMemberController@getProjectWithMembers');
    Route::get('/member/{member_id}', 'ProjectMemberController@getMemberWithProjects');
    Route::delete('/{project_id}', 'ProjectMemberController@removeMemberFromProject');
    Route::post('', 'ProjectMemberController@addMemberToProject');
});
