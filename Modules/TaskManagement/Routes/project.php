<?php

Route::group(['prefix' => 'project'], function () {
    Route::get('', 'ProjectController@getAllProject');
    Route::get('/{id}', 'ProjectController@getProject');
    Route::post('', 'ProjectController@createProject');
    Route::put('', 'ProjectController@updateProject');
    Route::delete('/{id}', 'ProjectController@deleteProject');
    Route::delete('/archive/{id}', 'ProjectController@archiveProject');
    Route::patch('/assign', 'ProjectController@assignProjectToOwnerOrManager');
});
