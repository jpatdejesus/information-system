<?php

namespace Modules\TaskManagement\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Modules\TaskManagement\Repositories\ProjectRepository;
use Modules\TaskManagement\Http\Requests\ProjectStoreRequest;

class ProjectController extends BaseController
{
    protected $projectRepository;

    /**
     * ProjectController constructor.
     *
     * @param ProjectRepository $projectRepository
     */
    public function __construct(ProjectRepository $projectRepository)
    {
        $this->projectRepository = $projectRepository;
    }

    /**
     * Get all Project.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllProject()
    {
        $response = $this->projectRepository->getAllProject();
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['GET_ALL_PROJECT_FAILED']);
    }

    /**
     * Create a Project.
     *
     * @param ProjectStoreRequest $projectStoreRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function createProject(ProjectStoreRequest $projectStoreRequest)
    {
        $response = $this->projectRepository->createProject($projectStoreRequest);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['CREATE_PROJECT_FAILED']);
    }

    /**
     * Get a Project.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProject($id)
    {
        $response = $this->projectRepository->getProject($id);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['GET_PROJECT_FAILED']);
    }

    /**
     * Update a Project.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProject(Request $request)
    {
        $response = $this->projectRepository->updateProject($request);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['UPDATE_PROJECT_FAILED']);
    }

    /**
     * Delete a Project.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteProject($id)
    {
        $response = $this->projectRepository->deleteProject($id);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], $this->projectRepository::VALIDATION_MESSAGES['DELETE_PROJECT_FAILED']);
    }

    /**
     * Archive a Project.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function archiveProject($id)
    {
        $response = $this->projectRepository->archiveProject($id);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['ARCHIVE_PROJECT_FAILED']);
    }

    /**
     * Assign a Project to a Owner or Manager.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignProjectToOwnerOrManager(Request $request)
    {
        $response = $this->projectRepository->assignProjectToOwnerOrManager($request);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['PROJECT_ASSIGNMENT_FAILED']);
    }
}
