<?php

namespace Modules\TaskManagement\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Modules\TaskManagement\Repositories\OwnerRepository;
use Modules\TaskManagement\Http\Requests\OwnerStoreRequest;

class OwnerController extends BaseController
{
    protected $ownerRepository;

    /**
     * OwnerController constructor.
     *
     * @param OwnerRepository $ownerRepository
     */
    public function __construct(OwnerRepository $ownerRepository)
    {
        $this->ownerRepository = $ownerRepository;
    }

    /**
     * Get all Owner.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllOwner()
    {
        $response = $this->ownerRepository->getAllOwner();
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['GET_ALL_OWNER_FAILED']);
    }

    /**
     * Create a Owner.
     *
     * @param OwnerStoreRequest $ownerStoreRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function createOwner(OwnerStoreRequest $ownerStoreRequest)
    {
        $response = $this->ownerRepository->createOwner($ownerStoreRequest);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['CREATE_OWNER_FAILED']);
    }

    /**
     * Get a Owner.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOwner($id)
    {
        $response = $this->ownerRepository->getOwner($id);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['GET_OWNER_FAILED']);
    }

    /**
     * Update a Owner.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateOwner(Request $request)
    {
        $response = $this->ownerRepository->updateOwner($request);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['UPDATE_OWNER_FAILED']);
    }

    /**
     * Delete a Owner.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteOwner($id)
    {
        $response = $this->ownerRepository->deleteOwner($id);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['DELETE_OWNER_FAILED']);
    }
}
