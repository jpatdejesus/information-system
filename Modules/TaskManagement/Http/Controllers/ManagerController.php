<?php

namespace Modules\TaskManagement\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Modules\TaskManagement\Repositories\ManagerRepository;
use Modules\TaskManagement\Http\Requests\ManagerStoreRequest;

class ManagerController extends BaseController
{
    protected $managerRepository;

    /**
     * ManagerController constructor.
     *
     * @param ManagerRepository $managerRepository
     */
    public function __construct(ManagerRepository $managerRepository)
    {
        $this->managerRepository = $managerRepository;
    }

    /**
     * Get all Manager.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllManager()
    {
        $response = $this->managerRepository->getAllManager();
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['GET_ALL_MANAGER_FAILED']);
    }

    /**
     * Create a Manager.
     *
     * @param ManagerStoreRequest $managerStoreRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function createManager(ManagerStoreRequest $managerStoreRequest)
    {
        $response = $this->managerRepository->createManager($managerStoreRequest);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['CREATE_MANAGER_FAILED']);
    }

    /**
     * Get a Manager.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getManager($id)
    {
        $response = $this->managerRepository->getManager($id);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['GET_MANAGER_FAILED']);
    }

    /**
     * Update a Manager.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateManager(Request $request)
    {
        $response = $this->managerRepository->updateManager($request);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['UPDATE_MANAGER_FAILED']);
    }

    /**
     * Delete a Manager.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteManager($id)
    {
        $response = $this->managerRepository->deleteManager($id);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], self::VALIDATION_MESSAGES['DELETE_MANAGER_FAILED']);
    }
}
