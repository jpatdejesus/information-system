<?php

namespace Modules\TaskManagement\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Modules\TaskManagement\Repositories\TaskRepository;
use Modules\TaskManagement\Http\Requests\TaskStoreRequest;

class TaskController extends BaseController
{
    private $taskRepository;

    /**
     * TaskController constructor.
     *
     * @param TaskRepository $taskRepository
     */
    public function __construct(TaskRepository $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    /**
     * Get all Task.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllTask()
    {
        $response = $this->taskRepository->getAllTask();
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], $this->taskRepository::VALIDATION_MESSAGES['GET_ALL_TASK_FAILED']);
    }

    /**
     * Create a Task.
     *
     * @param TaskStoreRequest $taskStoreRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function createTask(TaskStoreRequest $taskStoreRequest)
    {
        $response = $this->taskRepository->createTask($taskStoreRequest);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], $this->taskRepository::VALIDATION_MESSAGES['CREATE_TASK_FAILED']);
    }

    /**
     * Update a Task.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateTask(Request $request)
    {
        $response = $this->taskRepository->updateTask($request);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], $this->taskRepository::VALIDATION_MESSAGES['UPDATE_TASK_FAILED']);
    }

    /**
     * Delete a Task.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteTask($id)
    {
        $response = $this->taskRepository->deleteTask($id);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], $this->taskRepository::VALIDATION_MESSAGES['DELETE_TASK_FAILED']);
    }

    /**
     * Archive a Task.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function archiveTask($id)
    {
        $response = $this->taskRepository->archiveTask($id);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], $this->taskRepository::VALIDATION_MESSAGES['ARCHIVE_TASK_FAILED']);
    }

    /**
     * Get a Task.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTask($id)
    {
        $response = $this->taskRepository->getTask($id);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], $this->taskRepository::VALIDATION_MESSAGES['GET_TASK_FAILED']);
    }

    /**
     * Assign a Task to a User.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignTaskToUser(Request $request)
    {
        $response = $this->taskRepository->assignTaskToUser($request);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], $this->taskRepository::VALIDATION_MESSAGES['TASK_ASSIGNMENT_FAILED']);
    }

    /**
     * Start or End a Task.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function startOrEndTask(Request $request)
    {
        $response = $this->taskRepository->startOrEndTask($request);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], $this->taskRepository::VALIDATION_MESSAGES['START_END_TASK_FAILED']);
    }

    /**
     * Close a Task.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function closeTask(Request $request)
    {
        $response = $this->taskRepository->closeTask($request);
        if ($response) {
            return $this->sendResponseOk($response['data'], $response['message']);
        }

        return $this->sendBadRequest([], $this->taskRepository::VALIDATION_MESSAGES['CLOSE_TASK_FAILED']);
    }
}
