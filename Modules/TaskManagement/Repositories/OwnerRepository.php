<?php

namespace Modules\TaskManagement\Repositories;

use App\Repositories\Repository;
use Modules\TaskManagement\Entities\Owner;
use Modules\TaskManagement\Exceptions\RepositoryRequestException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class OwnerRepository extends Repository
{
    const VALIDATION_MESSAGES = [
        'GET_ALL_OWNER_SUCCESS' => 'List of Owners.',
        'GET_ALL_OWNER_FAILED' => 'Failed to Get All Owner.',
        'CREATE_OWNER_SUCCESS' => 'Owner created successfully.',
        'CREATE_OWNER_FAILED' => 'Failed to Create Owner.',
        'UPDATE_OWNER_SUCCESS' => 'Owner updated successfully.',
        'UPDATE_OWNER_FAILED' => 'Failed to Update Owner ID: ',
        'DELETE_OWNER_SUCCESS' => 'Owner deleted successfully.',
        'DELETE_OWNER_FAILED' => 'Failed to Delete Owner ID: ',
        'GET_OWNER_SUCCESS' => 'Owner request successful.',
        'GET_OWNER_FAILED' => 'Failed to Get Owner ID: ',
    ];

    protected $validCreateAttributes = [
        'first_name',
        'last_name',
        'position'
    ];

    public $response = [];

    public function model()
    {
        return Owner::class;
    }

    public function buildResponse($data = [], $message = null)
    {
        $this->response['data'] = $data;
        $this->response['message'] = $message;
    }

    public function getAllOwner()
    {
        $owners = $this->all();
        if ($owners) {
            $this->buildResponse($owners->toArray(), self::VALIDATION_MESSAGES['GET_ALL_OWNER_SUCCESS']);
            return $this->response;
        }

        throw new RepositoryRequestException(
            'No Record found.',
            self::VALIDATION_MESSAGES['GET_ALL_OWNER_FAILED']
        );
    }

    public function createOwner($request)
    {
        DB::beginTransaction();
        try {
            $params = $request->only($this->validCreateAttributes);
            if ($params) {
                $owner = $this->model->create($params);
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());

            throw new RepositoryRequestException(
                'Create failed.',
                self::VALIDATION_MESSAGES['CREATE_OWNER_FAILED']
            );
        }
        DB::commit();

        $this->buildResponse($owner->toArray(), self::VALIDATION_MESSAGES['CREATE_OWNER_SUCCESS']);
        return $this->response;
    }

    public function getOwner($id)
    {
        $owner = $this->find($id);
        if ($owner) {
            $this->buildResponse($owner->toArray(), self::VALIDATION_MESSAGES['GET_OWNER_SUCCESS']);
            return $this->response;
        }

        throw new RepositoryRequestException(
            'No Record found.',
            self::VALIDATION_MESSAGES['GET_OWNER_FAILED'] . $id . '.'
        );
    }

    public function updateOwner($request)
    {
        DB::beginTransaction();
        try {
            if ($this->verifyRecord($request['id'])) {
                $owner = $this->find($request['id']);
                $owner->update($request->only($this->validCreateAttributes));
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());

            throw new RepositoryRequestException(
                'Update failed.',
                self::VALIDATION_MESSAGES['UPDATE_OWNER_FAILED'] . $request['id'] . '.'
            );
        }
        DB::commit();

        $this->buildResponse($owner->toArray(), self::VALIDATION_MESSAGES['UPDATE_OWNER_SUCCESS']);
        return $this->response;
    }

    public function deleteOwner($id)
    {
        DB::beginTransaction();
        try {
            if ($this->verifyRecord($id)) {
                $owner = $this->find($id);
                $owner->delete();
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());

            throw new RepositoryRequestException(
                'Delete failed.',
                self::VALIDATION_MESSAGES['DELETE_OWNER_FAILED'] . $id . '.'
            );
        }
        DB::commit();

        $this->buildResponse($owner->toArray(), self::VALIDATION_MESSAGES['DELETE_OWNER_SUCCESS']);
        return $this->response;
    }
}
