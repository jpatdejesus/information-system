<?php

namespace Modules\TaskManagement\Repositories;

use App\Repositories\Repository;
use Modules\TaskManagement\Entities\Manager;
use Modules\TaskManagement\Exceptions\RepositoryRequestException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ManagerRepository extends Repository
{
    const VALIDATION_MESSAGES = [
        'GET_ALL_MANAGER_SUCCESS' => 'List of Managers.',
        'GET_ALL_MANAGER_FAILED' => 'Failed to Get All Manager.',
        'CREATE_MANAGER_SUCCESS' => 'Manager created successfully.',
        'CREATE_MANAGER_FAILED' => 'Failed to Create Manager.',
        'UPDATE_MANAGER_SUCCESS' => 'Manager updated successfully.',
        'UPDATE_MANAGER_FAILED' => 'Failed to Update Manager ID: ',
        'DELETE_MANAGER_SUCCESS' => 'Manager deleted successfully.',
        'DELETE_MANAGER_FAILED' => 'Failed to Delete Manager ID: ',
        'GET_MANAGER_SUCCESS' => 'Manager request successful.',
        'GET_MANAGER_FAILED' => 'Failed to Get Manager ID: ',
    ];

    protected $validCreateAttributes = [
        'first_name',
        'last_name',
        'position'
    ];

    public $response = [];

    public function model()
    {
        return Manager::class;
    }

    public function buildResponse($data = [], $message = null)
    {
        $this->response['data'] = $data;
        $this->response['message'] = $message;
    }

    public function getAllManager()
    {
        $managers = $this->all();
        if ($managers) {
            $this->buildResponse($managers->toArray(), self::VALIDATION_MESSAGES['GET_ALL_MANAGER_SUCCESS']);
            return $this->response;
        }

        throw new RepositoryRequestException(
            'No Record found.',
            self::VALIDATION_MESSAGES['GET_ALL_MANAGER_FAILED']
        );
    }

    public function createManager($request)
    {
        DB::beginTransaction();
        try {
            $params = $request->only($this->validCreateAttributes);
            if ($params) {
                $manager = $this->model->create($params);
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());

            throw new RepositoryRequestException(
                'Create failed.',
                self::VALIDATION_MESSAGES['CREATE_MANAGER_FAILED']
            );
        }
        DB::commit();

        $this->buildResponse($manager->toArray(), self::VALIDATION_MESSAGES['CREATE_MANAGER_SUCCESS']);
        return $this->response;
    }

    public function getManager($id)
    {
        $manager = $this->find($id);
        if ($manager) {
            $this->buildResponse($manager->toArray(), self::VALIDATION_MESSAGES['GET_MANAGER_SUCCESS']);
            return $this->response;
        }

        throw new RepositoryRequestException(
            'No Record found.',
            self::VALIDATION_MESSAGES['GET_MANAGER_FAILED'] . $id . '.'
        );
    }

    public function updateManager($request)
    {
        DB::beginTransaction();
        try {
            if ($this->verifyRecord($request['id'])) {
                $manager = $this->find($request['id']);
                $manager->update($request->only($this->validCreateAttributes));
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());

            throw new RepositoryRequestException(
                'Update failed.',
                self::VALIDATION_MESSAGES['UPDATE_MANAGER_FAILED'] . $request['id'] . '.'
            );
        }
        DB::commit();

        $this->buildResponse($manager->toArray(), self::VALIDATION_MESSAGES['UPDATE_MANAGER_SUCCESS']);
        return $this->response;
    }

    public function deleteManager($id)
    {
        DB::beginTransaction();
        try {
            if ($this->verifyRecord($id)) {
                $manager = $this->find($id);
                $manager->delete();
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());
            throw new RepositoryRequestException(
                'Delete failed.',
                self::VALIDATION_MESSAGES['DELETE_MANAGER_FAILED'] . $id . '.'
            );
        }
        DB::commit();

        $this->buildResponse($manager->toArray(), self::VALIDATION_MESSAGES['DELETE_MANAGER_SUCCESS']);
        return $this->response;
    }
}
