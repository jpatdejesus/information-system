<?php

namespace Modules\TaskManagement\Repositories;

use App\Repositories\Repository;
use Modules\TaskManagement\Entities\Project;
use Modules\TaskManagement\Exceptions\RepositoryRequestException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProjectRepository extends Repository
{
    const VALIDATION_MESSAGES = [
        'GET_ALL_PROJECT_SUCCESS' => 'List of Projects.',
        'GET_ALL_PROJECT_FAILED' => 'Failed to Get All Project.',
        'CREATE_PROJECT_SUCCESS' => 'Project created successfully.',
        'CREATE_PROJECT_FAILED' => 'Failed to Create Project.',
        'UPDATE_PROJECT_SUCCESS' => 'Project updated successfully.',
        'UPDATE_PROJECT_FAILED' => 'Failed to Update Project ID: ',
        'ARCHIVE_PROJECT_SUCCESS' => 'Project archived successfully.',
        'ARCHIVE_PROJECT_FAILED' => 'Failed to Archive Project ID: ',
        'DELETE_PROJECT_SUCCESS' => 'Project deleted successfully.',
        'DELETE_PROJECT_FAILED' => 'Failed to Delete Project ID: ',
        'GET_PROJECT_SUCCESS' => 'Project request successful.',
        'GET_PROJECT_FAILED' => 'Failed to Get Project ID: ',
        'ADD_MEMBER_TO_PROJECT_SUCCESS' => 'Add Member to Project successful.',
        'ADD_MEMBER_TO_PROJECT_FAILED' => 'Failed to Add Member to Project.',
        'PROJECT_ASSIGNMENT_SUCCESS' => 'Project assignment successful.',
        'PROJECT_ASSIGNMENT_FAILED' => 'Failed to assign Project ID: ',
    ];

    protected $validCreateAttributes = [
        'name',
        'description',
        'deadline',
        'owner_id',
        'manager_id',
    ];

    protected $validAssignAttributes = [
        'owner_id',
        'manager_id',
    ];

    public $response = [];

    public function model()
    {
        return Project::class;
    }

    public function buildResponse($data = [], $message = null)
    {
        $this->response['data'] = $data;
        $this->response['message'] = $message;
    }

    public function getAllProject()
    {
        $projects = $this->all();
        if ($projects) {
            $this->buildResponse($projects->toArray(), self::VALIDATION_MESSAGES['GET_ALL_PROJECT_SUCCESS']);
            return $this->response;
        }

        throw new RepositoryRequestException(
            'No Record found.',
            self::VALIDATION_MESSAGES['GET_ALL_PROJECT_FAILED']
        );
    }

    public function createProject($request)
    {
        DB::beginTransaction();
        try {
            $params = $request->only($this->validCreateAttributes);
            if ($params) {
                $project = $this->model->create($params);
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());

            throw new RepositoryRequestException(
                'Create failed.',
                self::VALIDATION_MESSAGES['CREATE_PROJECT_FAILED']
            );
        }
        DB::commit();

        $this->buildResponse($project->toArray(), self::VALIDATION_MESSAGES['CREATE_PROJECT_SUCCESS']);
        return $this->response;
    }

    public function getProject($id)
    {
        $project = $this->find($id);
        if ($project) {
            $this->buildResponse($project->toArray(), self::VALIDATION_MESSAGES['GET_PROJECT_SUCCESS']);
            return $this->response;
        }

        throw new RepositoryRequestException(
            'No Record found.',
            self::VALIDATION_MESSAGES['GET_PROJECT_FAILED'] . $id . '.'
        );
    }

    public function updateProject($request)
    {
        DB::beginTransaction();
        try {
            if ($this->verifyRecord($request['id'])) {
                $project = $this->find($request['id']);
                $project->update($request->only($this->validCreateAttributes));
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());
            
            throw new RepositoryRequestException(
                'Update failed.',
                self::VALIDATION_MESSAGES['UPDATE_PROJECT_FAILED'] . $request['id'] . '.'
            );
        }
        DB::commit();

        $this->buildResponse($project->toArray(), self::VALIDATION_MESSAGES['UPDATE_PROJECT_SUCCESS']);
        return $this->response;
    }

    public function deleteProject($id)
    {
        DB::beginTransaction();
        try {
            if ($this->verifyRecord($id)) {
                $project = $this->find($id);
                $project->forceDelete();
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());
            
            throw new RepositoryRequestException(
                'Delete failed.',
                self::VALIDATION_MESSAGES['DELETE_PROJECT_FAILED'] . $id . '.'
            );
        }
        DB::commit();

        $this->buildResponse($project->toArray(), self::VALIDATION_MESSAGES['DELETE_PROJECT_SUCCESS']);
        return $this->response;
    }

    public function archiveProject($id)
    {
        DB::beginTransaction();
        try {
            if ($this->verifyRecord($id)) {
                $project = $this->find($id);
                $project->delete();
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());
            
            throw new RepositoryRequestException(
                'Archive failed.',
                self::VALIDATION_MESSAGES['ARCHIVE_PROJECT_FAILED'] . $id . '.'
            );
        }
        DB::commit();

        $this->buildResponse($project->toArray(), self::VALIDATION_MESSAGES['ARCHIVE_PROJECT_SUCCESS']);
        return $this->response;
    }

    public function assignProjectToOwnerOrManager($request)
    {
        DB::beginTransaction();
        try {
            if ($this->verifyRecord($request['id'])) {
                $project = $this->find($request['id']);
                $project->update($request->only($this->validAssignAttributes));
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());
            
            throw new RepositoryRequestException(
                'Assignment failed.',
                self::VALIDATION_MESSAGES['PROJECT_ASSIGNMENT_FAILED'] . $request['id'] . '.'
            );
        }
        DB::commit();

        $this->buildResponse($project->toArray(), self::VALIDATION_MESSAGES['PROJECT_ASSIGNMENT_SUCCESS']);
        return $this->response;
    }
}
