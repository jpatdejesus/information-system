<?php

namespace Modules\TaskManagement\Repositories;

use App\Repositories\Repository;
use Modules\TaskManagement\Entities\Task;
use Modules\TaskManagement\Entities\TaskDetail;
use Modules\TaskManagement\Exceptions\RepositoryRequestException;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class TaskRepository extends Repository
{
    const VALIDATION_MESSAGES = [
        'GET_ALL_TASK_SUCCESS' => 'List of Tasks.',
        'GET_ALL_TASK_FAILED' => 'Failed to Get All Task.',
        'CREATE_TASK_SUCCESS' => 'Task created successfully.',
        'CREATE_TASK_FAILED' => 'Failed to Create Task.',
        'UPDATE_TASK_SUCCESS' => 'Task updated successfully.',
        'UPDATE_TASK_FAILED' => 'Failed to Update Task ID: ',
        'DELETE_TASK_SUCCESS' => 'Task deleted successfully.',
        'DELETE_TASK_FAILED' => 'Failed to Delete Task ID: ',
        'ARCHIVE_TASK_SUCCESS' => 'Task archived successfully.',
        'ARCHIVE_TASK_FAILED' => 'Failed to Archive Task ID: ',
        'GET_TASK_SUCCESS' => 'Task request successful.',
        'GET_TASK_FAILED' => 'Failed to Get Task ID: ',
        'TASK_ASSIGNMENT_SUCCESS' => 'Task assigment successful.',
        'TASK_ASSIGNMENT_FAILED' => 'Failed to Assign Task ID: ',
        'START_END_TASK_SUCCESS' => 'Start or End Task successful.',
        'START_END_TASK_FAILED' => 'Failed to start or end Task ID: ',
        'CLOSE_TASK_SUCCESS' => 'Closing of Task successful.',
        'CLOSE_TASK_FAILED' => 'Failed to close Task ID: ',
    ];

    protected $validTaskCreateAttributes = [
        'module_id',
        'user_id'
    ];

    protected $validTaskDetailCreateAttributes = [
        'task_id',
        'subject',
        'description',
        'priority',
        'status',
        'date_assigned',
        'deadline',
        'created_by',
        'assignee',
        'start_date',
        'end_date',
        'closed_by',
        'loe',
        'actual_hours',
        'remarks'
    ];

    protected $validTaskDetailUpdateAttributes = [
        'subject',
        'description',
        'priority',
        'status',
        'date_assigned',
        'deadline',
        'assignee',
        'start_date',
        'end_date',
        'closed_by',
        'loe',
        'actual_hours',
        'remarks'
    ];

    protected $selectFields = [
        'tasks.id',
        'task_details.task_id',
        'tasks.module_id',
        'tasks.user_id',
        'task_details.subject',
        'task_details.description',
        'task_details.priority',
        'task_details.status',
        'task_details.date_assigned',
        'task_details.deadline',
        'task_details.created_by',
        'task_details.assignee',
        'task_details.start_date',
        'task_details.end_date',
        'task_details.closed_by',
        'task_details.loe',
        'task_details.actual_hours',
        'task_details.remarks',
        'tasks.created_at'
    ];

    public $response = [];

    public function model()
    {
        return Task::class;
    }

    public function buildResponse($data = [], $message = null)
    {
        $this->response['data'] = $data;
        $this->response['message'] = $message;
    }

    public function getAllTask()
    {
        $tasks = $this->model->select($this->selectFields)
            ->leftJoin('task_details', 'tasks.id', '=', 'task_details.task_id')
            ->get();

        if ($tasks) {
            $this->buildResponse($tasks->toArray(), self::VALIDATION_MESSAGES['GET_ALL_TASK_SUCCESS']);
            return $this->response;
        }

        throw new RepositoryRequestException(
            'No Record found.',
            self::VALIDATION_MESSAGES['GET_ALL_TASK_FAILED']
        );
    }

    public function createTask($request)
    {
        DB::beginTransaction();
        try {
            $task_params = $request->only($this->validTaskCreateAttributes);
            $task_detail_params = $request->only($this->validTaskDetailCreateAttributes);

            if ($task_params && $task_detail_params) {
                $task = $this->model->create($task_params);
                $task_detail_params['task_id'] = $task->id;
                // $task_detail_params['created_by'] = Auth::id();
                $task_detail = TaskDetail::create($task_detail_params);

                $data['task'] = $task;
                $data['task_details'] = $task_detail;
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());
            
            throw new RepositoryRequestException(
                'Create failed.',
                self::VALIDATION_MESSAGES['CREATE_TASK_FAILED']
            );
        }
        DB::commit();

        $this->buildResponse($data, self::VALIDATION_MESSAGES['CREATE_TASK_SUCCESS']);
        return $this->response;
    }

    public function updateTask($request)
    {
        DB::beginTransaction();
        try {
            if ($this->verifyRecord($request['id'])) {
                $task = $this->find($request['id']);
                $task_detail = TaskDetail::where('task_id', $request['id']);

                $task->update($request->only($this->validTaskCreateAttributes));
                $task_detail->update($request->only($this->validTaskDetailUpdateAttributes));

                $data['task'] = $task;
                $data['task_details'] = $task_detail;
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());
            
            throw new RepositoryRequestException(
                'Update failed.',
                self::VALIDATION_MESSAGES['UPDATE_TASK_FAILED'] . $request['task_id'] . '.'
            );
        }
        DB::commit();

        $this->buildResponse($data, self::VALIDATION_MESSAGES['UPDATE_TASK_SUCCESS']);
        return $this->response;
    }

    public function deleteTask($id)
    {
        DB::beginTransaction();
        try {
            if ($this->verifyRecord($id)) {
                $task = $this->find($id);
                $task->forceDelete();
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());
            
            throw new RepositoryRequestException(
                'Delete failed.',
                self::VALIDATION_MESSAGES['DELETE_TASK_FAILED'] . $id . '.'
            );
        }
        DB::commit();

        $this->buildResponse($task->toArray(), self::VALIDATION_MESSAGES['DELETE_TASK_SUCCESS']);
        return $this->response;
    }

    public function archiveTask($id)
    {
        DB::beginTransaction();
        try {
            if ($this->verifyRecord($id)) {
                $task = $this->find($id);
                $task->delete();
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());
            
            throw new RepositoryRequestException(
                'Archive failed.',
                self::VALIDATION_MESSAGES['ARCHIVE_TASK_FAILED'] . $id . '.'
            );
        }
        DB::commit();

        $this->buildResponse($task->toArray(), self::VALIDATION_MESSAGES['ARCHIVE_TASK_SUCCESS']);
        return $this->response;
    }

    public function getTask($id)
    {
        $task = $this->model->select($this->selectFields)
            ->leftJoin('task_details', 'tasks.id', '=', 'task_details.task_id')
            ->where('tasks.id', $id)
            ->first();

        if ($task) {
            $this->buildResponse($task->toArray(), self::VALIDATION_MESSAGES['GET_TASK_SUCCESS']);
            return $this->response;
        }

        throw new RepositoryRequestException(
            'No Record found.',
            self::VALIDATION_MESSAGES['GET_TASK_FAILED'] . $id . '.'
        );
    }

    public function assignTaskToUser($request)
    {
        DB::beginTransaction();
        try {
            if ($this->verifyRecord($request['id'])) {
                $task = $this->find($request['id']);
                $task_detail = TaskDetail::where('task_id', $request['id']);

                $task->update(['user_id' => $request['user_id']]);
                $task_detail->update([
                    'date_assigned' => Carbon::now(),
                    // 'assignee' => Auth::id()
                ]);
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());
            
            throw new RepositoryRequestException(
                'Assignment failed.',
                self::VALIDATION_MESSAGES['TASK_ASSIGNMENT_FAILED'] . $request['id'] . ' to User ID: ' . $request['user_id'] . '.'
            );
        }
        DB::commit();

        $this->buildResponse($task->toArray(), self::VALIDATION_MESSAGES['TASK_ASSIGNMENT_SUCCESS']);
        return $this->response;
    }

    public function startOrEndTask($request)
    {
        DB::beginTransaction();
        try {
            if ($this->verifyRecord($request['id'])) {
                $field = $request['action'] . '_date';
                $task_detail = TaskDetail::where('task_id', $request['id']);
                $task_detail->update(["$field" => Carbon::now()]);
                $data['task'] = 'Task ID: ' . $request['id'] . ' successfully ' .  $request['action'] . 'ed.';
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());
            
            throw new RepositoryRequestException(
                'Start or End Task failed.',
                self::VALIDATION_MESSAGES['START_END_TASK_FAILED'] . $request['id'] . '.'
            );
        }
        DB::commit();

        $this->buildResponse($data, self::VALIDATION_MESSAGES['START_END_TASK_SUCCESS']);
        return $this->response;
    }

    public function closeTask($request)
    {
        DB::beginTransaction();
        try {
            if ($this->verifyRecord($request['id'])) {
                $task_detail = TaskDetail::where('task_id', $request['id']);
                $task_detail->update([
                    'closed_by' => $request['user_id'], // Change to Auth::id()
                    'remarks' => $request['remarks'],
                    'status' => 'DONE'
                ]);
                $data['task'] = 'Task ID: ' . $request['id'] . ' successfully ' .  'closed by User ID: ' . $request['user_id']  . '.';
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());
            
            throw new RepositoryRequestException(
                'Closing of Task failed.',
                self::VALIDATION_MESSAGES['CLOSE_TASK_FAILED'] . $request['id'] . '.'
            );
        }
        DB::commit();

        $this->buildResponse($data, self::VALIDATION_MESSAGES['CLOSE_TASK_SUCCESS']);
        return $this->response;
    }
}
