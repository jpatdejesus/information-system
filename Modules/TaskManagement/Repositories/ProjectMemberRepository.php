<?php

namespace Modules\TaskManagement\Repositories;

use App\Repositories\Repository;
use Modules\TaskManagement\Entities\ProjectMember;
use Modules\TaskManagement\Entities\Project;
use Modules\TaskManagement\Entities\UserDetail;
use Modules\TaskManagement\Exceptions\RepositoryRequestException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProjectMemberRepository extends Repository
{
    const VALIDATION_MESSAGES = [
        'GET_MEMBER_WITH_PROJECTS_SUCCESS' => 'Member with Projects request successfully.',
        'GET_MEMBER_WITH_PROJECTS_FAILED' => 'Failed to Get Member with Projects',
        'GET_PROJECT_WITH_MEMBERS_SUCCESS' => 'Project with Member request successful.',
        'GET_PROJECT_WITH_MEMBERS_FAILED' => 'Failed to Get Project with Members',
        'ADD_MEMBER_TO_PROJECT_SUCCESS' => 'Add Member to Project successful.',
        'ADD_MEMBER_TO_PROJECT_FAILED' => 'Member ID: ',
        'REMOVE_MEMBER_FROM_PROJECT_SUCCESS' => 'Remove Member from Project successful.',
        'REMOVE_MEMBER_FROM_PROJECT_FAILED' => 'Member ID: ',
    ];

    public $response = [];

    public function model()
    {
        return ProjectMember::class;
    }

    public function buildResponse($data = [], $message = null)
    {
        $this->response['data'] = $data;
        $this->response['message'] = $message;
    }

    public function addMemberToProject($request)
    {
        DB::beginTransaction();
        try {
            if (!$this->checkMemberInProject($request['member_id'], $request['project_id'])) {
                $params = [
                    'project_id' => $request['project_id'],
                    'member_id' => $request['member_id']
                ];
                $project_member = $this->model->create($params);
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());

            throw new RepositoryRequestException(
                'Failed to Add Member to Project.',
                self::VALIDATION_MESSAGES['ADD_MEMBER_TO_PROJECT_FAILED'] . $request['member_id'] . ' to Project ID: ' . $request['project_id'] . '.'
            );
        }
        DB::commit();

        $this->buildResponse($project_member->toArray(), self::VALIDATION_MESSAGES['ADD_MEMBER_TO_PROJECT_SUCCESS']);
        return $this->response;
    }

    public function checkMemberInProject($member_id, $project_id)
    {
        $project_member = $this->findWhere([
            'project_id' => $project_id,
            'member_id' => $member_id
        ]);

        if ($project_member->toArray()) {
            return true;
        }

        return false;
    }

    public function removeMemberFromProject($request, $project_id)
    {
        DB::beginTransaction();
        try {
            $project_member = $this->findWhere([
                'project_id' => $project_id,
                'member_id' => $request['member_id']
            ])->first();

            if ($project_member) {
                $project_member->delete();
            }
        } catch (\Exception $e) {
            DB::rollback();
            Log::error(__CLASS__ . '::' . __METHOD__. '(Line: ' . __LINE__. ') -> ' . $e->getMessage());

            throw new RepositoryRequestException(
                'Failed to Remove Member from Project.',
                self::VALIDATION_MESSAGES['REMOVE_MEMBER_FROM_PROJECT_FAILED'] . $request['member_id'] . ' to Project ID: ' . $project_id . '.'
            );
        }
        DB::commit();

        $this->buildResponse($project_member->toArray(), self::VALIDATION_MESSAGES['REMOVE_MEMBER_FROM_PROJECT_SUCCESS']);
        return $this->response;
    }

    public function getProjectWithMembers($request)
    {
        $project_name = Project::select('name')->where('id', $request['id'])->first();
        $members = $this->model->select(
            'user_details.first_name',
            'user_details.last_name'
        )
        ->leftJoin('projects', 'projects.id', '=', 'project_members.project_id')
        ->leftJoin('user_details', 'user_details.id', '=', 'project_members.member_id')
        ->where('project_members.project_id', $request['id'])
        ->get();

        $data['Project'] = $project_name;
        $data['Members'] = $members->toArray();

        if ($data) {
            $this->buildResponse($data, self::VALIDATION_MESSAGES['GET_PROJECT_WITH_MEMBERS_SUCCESS']);
            return $this->response;
        }

        throw new RepositoryRequestException(
            'No Record found.',
            self::VALIDATION_MESSAGES['GET_PROJECT_WITH_MEMBERS_FAILED']
        );
    }

    public function getMemberWithProjects($member_id)
    {
        $member_name = UserDetail::select('first_name', 'last_name')->where('id', $member_id)->first();
        $projects = $this->model->select('projects.name')
            ->leftJoin('projects', 'projects.id', '=', 'project_members.project_id')
            ->leftJoin('user_details', 'user_details.id', '=', 'project_members.member_id')
            ->where('project_members.member_id', $member_id)
            ->get();

        $data['Member'] = $member_name;
        $data['Projects'] = $projects->toArray();

        if ($data) {
            $this->buildResponse($data, self::VALIDATION_MESSAGES['GET_MEMBER_WITH_PROJECTS_SUCCESS']);
            return $this->response;
        }

        throw new RepositoryRequestException(
            'No Record found.',
            self::VALIDATION_MESSAGES['GET_MEMBER_WITH_PROJECTS_FAILED']
        );
    }
}
