<?php

namespace Modules\TaskManagement\Entities;

use Illuminate\Database\Eloquent\Model;

class TaskDetail extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'task_id',
        'subject',
        'description',
        'priority',
        'status',
        'data_assigned',
        'deadline',
        'created_by',
        'assignee',
        'start_date',
        'end_date',
        'closed_by',
        'loe',
        'actual_hours',
        'remarks'
    ];
}
