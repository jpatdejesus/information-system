<?php

namespace Modules\Conference\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Conference\Entities\Schedule;

class ScheduleController extends Controller
{
    /**
     * Browse a listing of the resource.
     * @return Response
     */
    public function browse()
    {
        $schedules = Schedule::all();

        return $schedules;
    }

    /**
     * Read a specified resource.
     * @param int $id
     * @return Response
     */
    public function read($id)
    {
        $schedule = Schedule::find($id);

        return $schedule;
    }

    /**
     * Edit a specified resource in storage.
     * @param Request $request
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $schedule = Schedule::findOrFail($id);
        $schedule->update($request->all());

        return response()->json($schedule, 200);
    }

    /**
     * Add a new resource.
     * @return Response
     */
    public function add(Request $request)
    {
        $schedule = Schedule::create($request->all());

        return response()->json($schedule, 201);
    }

    /**
     * Delete a specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function delete($id)
    {
        $schedule = Schedule::findOrFail($id);
        $schedule->delete();

        return response()->json(null, 204);
    }
}
