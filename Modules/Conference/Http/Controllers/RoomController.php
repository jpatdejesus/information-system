<?php

namespace Modules\Conference\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Conference\Entities\Room;

class RoomController extends Controller
{
    /**
     * Browse a listing of the resource.
     * @return Response
     */
    public function browse()
    {
        $rooms = Room::all();

        return $rooms;
    }

    /**
     * Read a specified resource.
     * @param int $id
     * @return Response
     */
    public function read($id)
    {
        $room = Room::find($id);

        return $room;
    }

    /**
     * Edit a specified resource in storage.
     * @param Request $request
     * @return Response
     */
    public function edit(Request $request, $id)
    {
        $room = Room::findOrFail($id);
        $room->update($request->all());

        return response()->json($room, 200);
    }

    /**
     * Add a new resource.
     * @return Response
     */
    public function add(Request $request)
    {
        $room = Room::create($request->all());

        return response()->json($room, 201);
    }

    /**
     * Delete a specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function delete($id)
    {
        $room = Room::findOrFail($id);
        $room->delete();

        return response()->json(null, 204);
    }
}
