<?php

namespace Modules\FormManagement\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class FileFormRequest extends FormRequest
{
    const ROUTE_UPDATE_FORM_REQUEST = 'Update request form';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $route = request()->route();
        $routeName = $route->action['name'];
        $return = [];

        switch ($this->method()) {
            case 'POST':
                if ($routeName !== self::ROUTE_UPDATE_FORM_REQUEST) {
                    $return = [
                        'employee_name' => 'required',
                        'employee_number' => 'required',
                        'position' => 'required',
                        'department' => 'required',
                        'date_hired' => 'required:date',
                        'type_of_request' => 'required',
                    ];
                } else {
                    $return = [
                        'id' => 'required|exists:file_request_forms,id',
                        'employee_name' => 'required',
                        'employee_number' => 'required',
                        'position' => 'required',
                        'department' => 'required',
                        'date_hired' => 'required|date',
                        'approved_by' => 'required',
                        'checked_by' => 'required',
                        'type_of_request' => 'required',
                    ];
                }
                break;
        }
        
        return $return;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            '*.required' => ':attribute is required',
        ];
    }
}
