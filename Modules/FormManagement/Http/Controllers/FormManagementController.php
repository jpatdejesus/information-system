<?php

namespace Modules\FormManagement\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Http\Controllers\BaseController;
use Modules\FormManagement\Http\Requests\FileFormRequest;
use Modules\FormManagement\Repositories\FileFormRepository;

class FormManagementController extends BaseController
{

    /**
     * @var mixed
     */
    private $fileFormRepository;

    /**
     * @param FileFormRepository $fileFormRepository
     */
    public function __construct(FileFormRepository $fileFormRepository)
    {
        $this->fileFormRepository = $fileFormRepository;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return $this->fileFormRepository->getAllFileRequestForm();
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(FileFormRequest $request)
    {
        return $this->fileFormRepository->addFileRequestForm($request);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(FileFormRequest $request)
    {
        return view('accountmanagement::store');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show()
    {
        return view('accountmanagement::show');
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(FileFormRequest $request)
    {
        return $this->fileFormRepository->updateFileRequestForm($request);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        return $this->fileFormRepository->deleteFileRequestForm($id);
    }
}
