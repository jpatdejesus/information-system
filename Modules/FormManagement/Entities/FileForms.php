<?php

namespace Modules\FormManagement\Entities;

use Illuminate\Database\Eloquent\Model;

class FileForms extends Model
{
    const REQUEST_FORM_STATUS = 'PENDING';
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'file_request_forms';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_name',
        'employee_number',
        'position',
        'department',
        'date_hired',
        'status',
        'type_of_request',
        'request_information',
        'approved_by',
        'checked_by',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
}
