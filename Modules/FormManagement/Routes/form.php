<?php



// Route::group(['prefix' => 'request', 'middleware' => ['auth:api', 'role:admin|employee']], function () {
Route::group(['prefix' => 'request'], function () {
    Route::get('/', [
        'name' => 'Get all request form',
        'uses' => 'FormManagementController@index',
    ]);

    Route::post('/add', [
        'name' => 'Add request form',
        'uses' => 'FormManagementController@create',
    ]);

    Route::put('/update', [
        'name' => 'Update request form',
        'uses' => 'FormManagementController@update',
    ]);

    Route::delete('/delete/{id}', [
        'name' => 'Delete request form',
        'uses' => 'FormManagementController@destroy',
    ]);
});


