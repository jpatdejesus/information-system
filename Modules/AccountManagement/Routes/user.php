<?php

Route::group(['prefix' => 'login'], function () {
    Route::post('', [
        'uses' => 'AuthController@login',
    ]);
});

//User Routes
Route::get('users', 'UserController@show')
    ->middleware(['auth:api', 'role:admin|employee']);

Route::group(['prefix' => 'user', 'middleware' => ['auth:api', 'role:admin'] ], function () {
    Route::post('/add', [
        'uses' => 'UserController@create',
    ]);

    Route::post('/update', [
        'uses' => 'UserController@updateUser',
    ]);

    Route::post('/{id}', [
        'uses' => 'UserController@showUserDetails',
    ]);
});

//User Details Routes
Route::group(['prefix' => 'user_details', 'middleware' => ['auth:api', 'role:admin'] ], function () {
    Route::post('/update', [
        'uses' => 'UserController@updateUserDetails',
    ]);

    Route::post('', [
        'uses' => 'UserController@showUserDetails',
    ]);
});

