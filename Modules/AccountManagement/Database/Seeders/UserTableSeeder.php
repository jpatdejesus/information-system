<?php

namespace Modules\AccountManagement\Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\AccountManagement\Entities\User;
use Modules\AccountManagement\Entities\UserDetail;

class UserTableSeeder extends Seeder
{
    const ADMINISTRATOR_ID = 1;

    protected $user;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call("OthersTableSeeder");

        //user
        $this->storeUser();

        //user details
        $this->storeUserDetails();
    }

    private function storeUser()
    {
        $user = new User();

        $user->email = 'admin@nomail.com';
        $user->username = 'admin123';
        $user->password = bcrypt('password123!');
        $user->status = 'ACTIVE';
        $user->is_notifiable = '1';
        $user->created_by = self::ADMINISTRATOR_ID;
        $user->updated_by = self::ADMINISTRATOR_ID;

        $user->save();

        $this->setUser($user);

        $user->assignRole('admin');
    }

    private function getUser()
    {
        return $this->user;
    }

    private function setUser($user)
    {
        $this->user = $user;
    }

    private function storeUserDetails()
    {
        $userDetails = new UserDetail();

        $userDetails->user_id = $this->getUser()->id;
        $userDetails->first_name = 'admin';
        $userDetails->middle_name = 'istra';
        $userDetails->last_name = 'tion';
        $userDetails->nickname = 'admin';
        $userDetails->date_hired = Carbon::now();
        $userDetails->birth_date = Carbon::create('1991', '01', '12', '01', '00', '00');
        $userDetails->mobile_number = '09097897822';
        $userDetails->level = 1;
        $userDetails->section = '';
        $userDetails->province = 'Metro Manila';
        $userDetails->city = 'Makati';
        $userDetails->town = 'magallanes';
        $userDetails->job_title = 'Developer';
        $userDetails->team_id = '1';
        $userDetails->shift = '9am - 7pm';
        $userDetails->tl_id = '1';

        $userDetails->save();
    }
}
