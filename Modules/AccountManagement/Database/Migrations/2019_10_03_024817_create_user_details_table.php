<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_details', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('user_id')->unsigned()->unique()->nullable();
            $table->string('photo')->nullable();
            $table->string('first_name', 50)->nullable();
            $table->string('middle_name', 50)->nullable();
            $table->string('last_name', 50)->nullable();
            $table->string('nickname', 10)->nullable();
            $table->date('date_hired')->nullable();
            $table->date('birth_date')->nullable();
            $table->string('mobile_number', 20)->nullable();
            $table->string('level', 50)->nullable();
            $table->string('section', 50)->nullable();
            $table->string('province', 100)->nullable();
            $table->string('city', 50)->nullable();
            $table->string('town', 50)->nullable();
            $table->enum('job_title', array_values(config('employee.job_titles')));
            $table->unsignedInteger('team_id')->nullable();
            $table->enum('shift', array_values(config('employee.shifts')));
            $table->integer('tl_id')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_details');
    }
}
