<?php

namespace Module\AccountManagement\Exceptions;

use Illuminate\Http\Response;
use App\Exceptions\BaseException;
use Illuminate\Support\Facades\Log;

class UnauthorizedException extends BaseException
{
    private $errors;

    public function __construct($message)
    {
        $this->setMessage($message);
    }

    public function report()
    {
        $message = $this->getMessage();

        $this->setMessage(self::BAD_REQUEST_ERROR_MESSAGE);

        Log::error("[REPOSITORY_REQUEST_EXCEPTION] - {$message}");
    }

    public static function getStatusCode()
    {
        return Response::HTTP_UNAUTHORIZED;
    }

    /**
     * @return array
     * @throws BaseException
     */
    public function setParams()
    {
        return [
            'message' => $this->getMessage(),
        ];
    }
}
