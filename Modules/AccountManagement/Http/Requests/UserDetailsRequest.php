<?php


namespace Modules\AccountManagement\Http\Request;

use Auth;
use Illuminate\Validation\Rule;
use App\Http\Requests\BaseRequest;
use App\Providers\ValidatorServiceProvider;

class UserDetailsRequestRequest extends BaseRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return $this->userDetailsRules();
    }

    protected function userDetailsRules()
    {
        $basic = [
            'first_name' => "required|between:1,50|regex:/^[A-Za-z \'\.\-\p{Han}]+$/u",
            'middle_name' => "required|between:1,50|regex:/^[A-Za-z \'\.\-\p{Han}]+$/u",
            'last_name' => "required|between:1,50|regex:/^[A-Za-z \'\.\-\p{Han}]+$/u",
            'photo' => "between:0,191",
            'nickname' => "between:0,10",
            'date_hired',
            'birth_date',
            'mobile_number'  => "between:0,20",
            'level' => "required|between:1,50",
            'section' => "required|between:1,50",
            'province'=> "required|between:1,100",
            'city'=> "required|between:0,50",
            'town'=> "required|between:0,50",
            'job_title'=> "required",
            'team_id' => "between:0,10",
            'shift' => "required",
            'tl_id' => "between:0,11",
        ];

        return $basic;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'first_name.regex' => 'The first name field may have letters, periods and dashes only.',
            'middle_name.regex' => 'The middle name field may have letters, periods and dashes only.',
            'last_name.regex' => 'The last name field may have letters, periods and dashes only.',
        ];
    }
}
